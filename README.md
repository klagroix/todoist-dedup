# Todoist-Dedup
Removes duplicate entries from a todoist project

## Usage

```
docker run -d \
 --name=todoist-dedup \
 -e API_TOKEN=<APITOKEN> \
 -e PROJECT_NAME=<PROJECT> \
 jovocop/todoist-dedup
 ```

### Environment variables

* `API_TOKEN` - Your todoist API token. To retrieve this, open Todoist Settings -> Integrations -> API token
* `PROJECT_NAME` - The name of the project in todoist to deduplicate
* (optional) `IGNORE_CHECKED` - Default: True. Set to False if you want to deduplicate already checked items as well
* (optional) `RUN_FREQUENCY_MIN` - Default: 10. Change this to the desired check interval (in minutes)

##  Build

GitLab CI automatically deploys new containers to https://hub.docker.com/repository/docker/jovocop/todoist-dedup

To build this manually, run `docker build -t todoist-dedup .`


## Libraries
Third Party libraries required are documneted in `requirements.txt`

## References

The following guides were used:
* https://developer.todoist.com/sync/v8/?python#getting-started
