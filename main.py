#!/usr/bin/env python

# Guides

"""Deduplicates from a todoist project
"""

from todoist.api import TodoistAPI
import schedule
import inflect
import os
import sys
import datetime
import time

TOOL_NAME = "todoist-dedup"

API_TOKEN = os.environ.get('API_TOKEN') # Todoist Settings -> Integrations -> API token
PROJECT_NAME = os.environ.get('PROJECT_NAME')
IGNORE_CHECKED = os.environ.get('IGNORE_CHECKED', True) # If True, we'll only process items that are not checked (i.e. still active in the list)
RUN_FREQUENCY_MIN = os.environ.get('RUN_FREQUENCY_MIN', 10) # How often we should run the check

def get_project(name, case_sensitive=False):
    """Returns a project dictionary (from the API) which matches the provided name"""
    projects = api.state['projects']

    for c_project in projects:
        # print("PROJECT: {0}".format(c_project))
        # print("Name: {0}".format(c_project["name"]))

        proj_name = c_project["name"]

        if case_sensitive and proj_name == name:
            # print("Found match")
            return c_project
        elif not case_sensitive and proj_name.lower() == name.lower():
            # print("Found match")
            return c_project

    return None

def get_items_for_project(project_id):
    """Gets all items for a given project id"""
    items = []

    for item in api.items.all():
        if item["project_id"] == project_id:
            if IGNORE_CHECKED and item["checked"]:
                continue
            items.append(item)

    return items

def delete_items(items):
    """Deletes items from the provided list"""

    if len(items) == 0:
        return
    for c_item in items:
        print("Deleting item with ID: {0} and content: {1}".format(c_item['id'], c_item['content']))
        c_item.delete()
    api.commit()

def finish_pass():
    print("Finished {0} @ {1}".format(TOOL_NAME, datetime.datetime.now()))
    #sys.exit(0)
    return 0

def finish_fail():
    print("Finished {0} @ {1}".format(TOOL_NAME, datetime.datetime.now()))
    #sys.exit(1)
    return 1

def get_singluar(word):
    """Returns the singular of a string. If we can't find the singular, we'll return the text as given"""
    converted = plural.singular_noun(word)
    if converted is False or converted is None:
        return word
    return converted



def run():
    print("Entering {0} @ {1}".format(TOOL_NAME, datetime.datetime.now()))
    
    # Init the API
    print("Initializing the API...", end='')
    api.sync()
    print("Done")
    
    # Get the project we care about
    print("Getting project {0}...".format(PROJECT_NAME), end='')
    project = get_project(PROJECT_NAME)
    if project is None:
        print("ERROR: Couldn't find project")
        finish_fail()
    print("Done")
    
    project_id = project["id"]
    
    # Iterate through items in the project. Store a list of duplicates
    print("Getting all items for the project...", end='')
    items = get_items_for_project(project_id)
    print("Done")
    
    non_duplicates = []
    duplicates = []
    
    
    # Iterate over all items and sort them into non_duplicates or duplicates list
    for c_item in items:
        # check if item['content'] is in the non_duplicates list
        found_matches = list(filter(lambda item: get_singluar(item['content']).lower() == get_singluar(c_item['content']).lower(), non_duplicates))
    
        # If we have a match, this current item is a duplicate. Add to duplicates list
        # if we don't have a match, this item is unique. Add to non-duplicates list
        if len(found_matches) == 0:
            # Unique (so far)
            non_duplicates.append(c_item)
        else:
            # Not unique. Add to duplicates list
            duplicates.append(c_item)
    
    print("We have {0} unique items".format(len(non_duplicates)))
    print("We have {0} duplicate items".format(len(duplicates)))
    
    delete_items(duplicates)
    
    finish_pass()

if API_TOKEN is None:
    print("ERROR: No API_TOKEN provided")
    sys.exit(1)

if PROJECT_NAME is None:
    print("ERROR: No PROJECT_NAME provided")
    sys.exit(1)


print("Starting container ({0}). Setting job to run every {1} minutes".format(datetime.datetime.now(), RUN_FREQUENCY_MIN))

# Set the API
api = TodoistAPI(API_TOKEN)

# Initalize inflect
plural = inflect.engine()

# Set the schedule
schedule.every(RUN_FREQUENCY_MIN).minutes.do(run)

# run schedules
while True:
    schedule.run_pending()
    time.sleep(1)